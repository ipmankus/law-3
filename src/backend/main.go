package main

import (
	"github.com/sirupsen/logrus"
	"github.com/labstack/echo"
	"github.com/streadway/amqp"

	"strings"
	"math"
	"math/rand"
	"net/http"
	"os"
	"io/ioutil"
	"io"
	"bytes"
	"path/filepath"
	"compress/gzip"
	"fmt"
	
	"time"

)

const location = "userfile"
const npm = "1706028676"
const urlVerb = `{"url": "http://infralabs.cs.ui.ac.id:20736/files/%s"}`
const progressVerb = `{"status": "%f %%"}`
var ch = &amqp.Channel{}

func init(){
	conn, err := amqp.DialConfig("amqp://0806444524:0806444524@152.118.148.95:5672", amqp.Config{Vhost: "/0806444524"})
	ch, err = conn.Channel()
	rand.Seed(time.Now().UTC().UnixNano())
	err = ch.ExchangeDeclare(
		npm,
		"direct",
		true,
		false,
		false,       
		false,        
		nil,           
	)

	err = os.MkdirAll(location, 0644)
	if err != nil {
		logrus.Fatal(err)
	}
}

func UploadPost(c echo.Context) (err error) {
	file, err := c.FormFile("file")
	if err != nil {
		return err
	}

	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()


	bytes, err := ioutil.ReadAll(src)
	if err!=nil{
		return
	}
	
	filename := randStringRunes(16)
	
	randomToken :=c.Request().Header.Get("X-ROUTING-KEY")
	
	if err != nil {
		return
	}

	logrus.Println(len(bytes), randomToken)

	go compress(filename, randomToken, bytes)
	return  c.JSON(http.StatusOK, struct {Message string}{"OK!"})
}


func GetFile(c echo.Context) (err error) {
	filename := c.Param("filename")


	if strings.ContainsAny(filename, `./\`){
		err = echo.NewHTTPError(http.StatusBadRequest, `Bad Request: hacker jangan macam-macam`)
		return
	}

	data, err := ioutil.ReadFile(filepath.Join(location, filename)) 

	c.Response().Header().Set(echo.HeaderContentDisposition, "attachment;")
	err = c.String(http.StatusOK, string(data))
	return
}


var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
			b[i] = letterRunes[rand.Intn(len(letterRunes))]
    }
    return string(b)
}

func compress(filename string, randomToken string, data []byte){
	time.Sleep(2 * time.Second)


	var b bytes.Buffer
	var result []byte
	var err error

	gz := gzip.NewWriter(&b)
	if _, err = gz.Write(data); err != nil {
		logrus.Println(err)
		return
	}

	if err = gz.Flush(); err != nil {
		logrus.Println(err)
		return
	}

	if err = gz.Close(); err != nil {
		logrus.Println(err)
		return
	}

	result = b.Bytes()


	err = WriteFile(filepath.Join(location, filename), result, 0644, randomToken)

	accessUrl := fmt.Sprintf(urlVerb, filename)
	msg := amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		Timestamp:    time.Now(),
		ContentType:  "application/json",
		Body:         []byte(accessUrl),
	}
	
	
	err = ch.Publish(npm, randomToken, false, false,  msg)
	if err != nil {
		logrus.Println(err)
	}

	return
}


type PercentProxy struct {
	total int
	current int
	randomToken string
}

func (pp *PercentProxy) Write(p []byte) (int, error){
	pp.current += len(p)

	progress := fmt.Sprintf(progressVerb, math.Ceil( 100*float64(pp.current)/float64(pp.total)))
	msg := amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		Timestamp:    time.Now(),
		ContentType:  "application/json",
		Body:         []byte(progress),
	}
	
	err := ch.Publish(npm, pp.randomToken, false, false,  msg)
	if err != nil {
		logrus.Println(err)
	}

	logrus.Println(pp.current)
	return len(p), nil
}

func WriteFile(filename string, data []byte, perm os.FileMode, randomToken string) error {
	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, perm)
	if err != nil {
		return err
	}

	src := bytes.NewBufferString(string(data))
	logrus.Println(len(data))


	pp := &PercentProxy{total: len(data), current: 0, randomToken: randomToken}
	if _, err = io.Copy(f, io.TeeReader(src, pp)); err != nil {
		logrus.Println(err)
	}
	
	if err1 := f.Close(); err == nil {
		err = err1
	}
	return err
}




func main(){
	e := echo.New()
	e.POST("/upload", UploadPost)
	e.GET("/files/:filename", GetFile)
	logrus.Fatal(e.Start(":20736")) //10004
}