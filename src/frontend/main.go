package main

import (
	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"github.com/imroc/req"

	"html/template"
	"math/rand"
	"net/http"
	"os"
	"io"
)

type Input struct {
	RandomToken string
}

type Template struct {
	templateGET *template.Template
	templatePOST *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	if name == "GET" {
		return t.templateGET.Execute(w, data)
	} else {
		return t.templatePOST.Execute(w, data)
	}
}

func UploadGet(c echo.Context) (err error) {
	return c.Render(http.StatusOK, "GET", "")
}

func UploadPost(c echo.Context) (err error) {
	randomToken := randStringRunes(16)

	data := Input {
		RandomToken: randomToken,
	}

	file, err := c.FormFile("file")
	if err!=nil{
		return
	}

	src, err := file.Open()
	if err!=nil{
		return
	}
	defer src.Close()
	
	// bytes, err := ioutil.ReadAll(src)
	// if err!=nil{
	// 	return
	// }



	header := req.Header{
		"X-ROUTING-KEY": randomToken,
	}
	param :=  req.FileUpload{
		File:      src,
		FieldName: "file",       
		FileName:  "file",
	}
	
	// req.Param{
	// 	"bytes": string(bytes),
	// }

	// logrus.Println(len(string(bytes)))

	_, err = req.Post(urlServer2, header, param)

	if err!=nil{
		logrus.Println(err)
		return
	}

	return c.Render(http.StatusOK, "POST", data)
	
}

const urlServer2 = "http://infralabs.cs.ui.ac.id:20736/upload"
// const urlServer2 = "http://requestbin.net/r/zqt760zq"
var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randStringRunes(n int) string {
    b := make([]rune, n)
    for i := range b {
        b[i] = letterRunes[rand.Intn(len(letterRunes))]
    }
    return string(b)
}

func main(){
	e := echo.New()
	wd, _ := os.Getwd()	
	t := &Template{
		templatePOST: template.Must(template.ParseFiles(wd + "/src/frontend/template/stomp.html")),
		templateGET: template.Must(template.ParseFiles(wd + "/src/frontend/template/layout.html")),
	}
	e.Renderer = t

	e.GET("/upload", UploadGet)
	e.POST("/upload", UploadPost)
	logrus.Fatal(e.Start(":20735")) //10003

}