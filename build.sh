export GOPATH=$(pwd)
go get github.com/sirupsen/logrus
go get github.com/labstack/echo
go get github.com/streadway/amqp
go get github.com/imroc/req
go build $1